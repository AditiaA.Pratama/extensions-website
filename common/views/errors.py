"""Error handlers."""

from django.http import HttpResponse
from django.views.generic import TemplateView


class ErrorView(TemplateView):
    """Renders an error page."""

    template_name = 'common/error.html'
    status = 500

    def dispatch(self, request=None, *args, **kwargs):
        # TODO: respond as JSON when this is an XHR.
        if request is None or request.method in {'HEAD', 'OPTIONS'}:
            # Don't render templates in this case.
            return HttpResponse(status=self.status)

        # We allow any method for this view
        context = self.get_context_data(**kwargs)
        context['status'] = self.status
        response = self.render_to_response(context)
        response.status_code = self.status
        return response


def csrf_failure(request, reason=''):
    import django.views.csrf

    return django.views.csrf.csrf_failure(
        request, reason=reason, template_name='errors/403_csrf.html'
    )
