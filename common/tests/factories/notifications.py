from django.contrib.contenttypes.models import ContentType
from factory.django import DjangoModelFactory
import factory

RELATION_ALLOWED_MODELS = []


def generic_foreign_key_id_for_type_factory(generic_relation_type_field):
    def generic_foreign_key_id(obj):
        model = getattr(obj, generic_relation_type_field).model_class()
        related = ContentType.objects.get_for_model(model)
        return related.id

    return factory.LazyAttribute(generic_foreign_key_id)


class ContentTypeFactory(DjangoModelFactory):
    class Meta:
        model = ContentType
