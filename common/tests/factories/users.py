import random

from django.contrib.auth import get_user_model
from factory.django import DjangoModelFactory
import factory

from blender_id_oauth_client.models import OAuthUserInfo, OAuthToken

User = get_user_model()


class OAuthUserInfoFactory(DjangoModelFactory):
    class Meta:
        model = OAuthUserInfo

    oauth_user_id = factory.Sequence(lambda n: n + 899999)

    user = factory.SubFactory('common.tests.factories.users.UserFactory')


class OAuthUserTokenFactory(DjangoModelFactory):
    class Meta:
        model = OAuthToken

    user = factory.SubFactory('common.tests.factories.users.UserFactory')


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    id = factory.Sequence(lambda n: n + 999)  # Bumped to 1000 because there's a fixture with a pk=1

    full_name = factory.Faker('name')
    username = factory.LazyAttribute(
        lambda o: f'{o.full_name.replace(" ", "_")}#{random.randint(1, 9999):04}'
    )
    email = factory.LazyAttribute(lambda o: f'{o.username}@example.com')
    password = 'pass'

    oauth_tokens = factory.RelatedFactoryList(OAuthUserTokenFactory, factory_related_name='user')
    oauth_info = factory.RelatedFactory(OAuthUserInfoFactory, factory_related_name='user')
