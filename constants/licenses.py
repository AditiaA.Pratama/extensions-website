"""Lists licenses allowed for add-ons.

Slugs should be taken from https://spdx.org/licenses/
"""

from django.utils.translation import gettext_lazy as _


class LICENSE_GPL2:
    id = 2
    name = _('GNU General Public License v2.0')
    url = 'http://www.gnu.org/licenses/gpl-2.0.html'
    slug = 'SPDX:GPL-2.0-or-later'


class LICENSE_GPL3:
    id = 3
    name = _('GNU General Public License v3.0')
    url = 'http://www.gnu.org/licenses/gpl-3.0.html'
    slug = 'SPDX:GPL-3.0-or-later'


ALL_LICENSES = (
    LICENSE_GPL2,
    LICENSE_GPL3,
)
