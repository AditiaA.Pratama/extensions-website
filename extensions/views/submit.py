import logging

from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import reverse
from django.views.generic.edit import CreateView, FormView

from .mixins import OwnsFileMixin
from extensions.models import Version, Extension
from extensions.forms import (
    ExtensionUpdateForm,
    VersionForm,
    AddPreviewFormSet,
)
from files.forms import FileForm
from files.models import File

logger = logging.getLogger(__name__)


class UploadFileView(LoginRequiredMixin, CreateView):
    model = File
    template_name = 'extensions/submit.html'
    form_class = FileForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self):
        return reverse('extensions:submit-finalise', kwargs={'pk': self.object.pk})


class SubmitFileView(LoginRequiredMixin, OwnsFileMixin, FormView):
    template_name = 'extensions/submit_finalise.html'
    form_class = VersionForm

    def _get_extension(self) -> 'Extension':
        # Get the existing Extension and Version, if they were partially saved already
        existing_version = getattr(self.file, 'version', None)
        if existing_version:
            extension = existing_version.extension
            assert (
                extension.can_request_review
            ), 'TODO: cannot request review for extension: {extension.get_status_display}'
            assert not extension.authors.count() or extension.authors.filter(
                user__id=self.request.user.pk
            ), 'TODO: cannot request review for extension: maintained by someone else'
            logger.warning(
                'Found existing version pk=%s for file pk=%s',
                existing_version.pk,
                self.file.pk,
            )
            return extension

        parsed_extension_fields = self.file.parsed_extension_fields
        if parsed_extension_fields:
            # Try to look up extension by the same author and file info
            extension = (
                Extension.objects.authored_by(user_id=self.request.user.pk)
                .filter(type=self.file.type, **parsed_extension_fields)
                .first()
            )
            if extension:
                logger.warning(
                    'Found existing extension pk=%s for file pk=%s',
                    extension.pk,
                    self.file.pk,
                )
                return extension
        return Extension.objects.update_or_create(type=self.file.type, **parsed_extension_fields)[0]

    def _get_version(self, extension) -> 'Version':
        return Version.objects.update_or_create(
            extension=extension, file=self.file, **self.file.parsed_version_fields
        )[0]

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['instance'] = self._get_version(self.extension)
        return form_kwargs

    def get_initial(self):
        """Return initial values for the version, based on the file."""
        initial = super().get_initial()
        initial['file'] = self.file
        initial.update(**self.file.parsed_version_fields)
        return initial

    def get_context_data(self, form=None, extension_form=None, add_preview_formset=None, **kwargs):
        """Add all the additional forms to the context."""
        context = super().get_context_data(**kwargs)
        if not (add_preview_formset and extension_form):
            extension_form = ExtensionUpdateForm(instance=self.extension)
            add_preview_formset = AddPreviewFormSet(extension=self.extension, request=self.request)
        context['extension_form'] = extension_form
        context['add_preview_formset'] = add_preview_formset
        return context

    def post(self, request, *args, **kwargs):
        """Handle bound forms and valid/invalid logic with the extra forms."""
        form = self.get_form()
        extension_form = ExtensionUpdateForm(
            self.request.POST, self.request.FILES, instance=self.extension
        )
        add_preview_formset = AddPreviewFormSet(
            self.request.POST, self.request.FILES, extension=self.extension, request=self.request
        )
        if form.is_valid() and extension_form.is_valid() and add_preview_formset.is_valid():
            return self.form_valid(form, extension_form, add_preview_formset)
        return self.form_invalid(form, extension_form, add_preview_formset)

    @transaction.atomic
    def form_valid(self, form, extension_form, add_preview_formset):
        """Save all the forms in correct order.

        Extension must be saved first.
        """
        try:
            # Send the extension and version to the review
            extension_form.instance.status = extension_form.instance.STATUSES.AWAITING_REVIEW
            extension_form.save()
            self.extension.authors.add(self.request.user)
            add_preview_formset.save()
            form.save()
            return super().form_valid(form)
        except forms.ValidationError as e:
            if 'hash' in e.error_dict:
                add_preview_formset.forms[0].add_error('source', e.error_dict['hash'])
        return self.form_invalid(form, extension_form, add_preview_formset)

    def form_invalid(self, form, extension_form, add_preview_formset):
        return self.render_to_response(
            self.get_context_data(form, extension_form, add_preview_formset)
        )

    def get_success_url(self):
        return self.file.extension.get_manage_url()
