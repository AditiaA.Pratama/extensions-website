"""Contains views allowing developers to manage their add-ons."""
from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.shortcuts import get_object_or_404, reverse
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


from .mixins import ExtensionQuerysetMixin, OwnsFileMixin, MaintainedExtensionMixin
from extensions.forms import (
    EditPreviewFormSet,
    AddPreviewFormSet,
    ExtensionUpdateForm,
    VersionForm,
    DeleteViewForm,
)
from extensions.models import Extension, Version
from files.forms import FileForm
from files.models import File
from stats.models import ExtensionView
import ratings.models


class ExtensionDetailView(ExtensionQuerysetMixin, DetailView):
    model = Extension
    context_object_name = 'extension'
    template_name = 'extensions/detail.html'

    def get_queryset(self):
        """Allow logged in users to view unlisted add-ons in certain conditions.

        * maintainers should be able to preview their yet unlisted add-ons;
        * staff should be able to preview yet unlisted add-ons;
        """
        return self.get_extension_queryset()

    def get_object(self, queryset=None):
        """Record a page view when returning the Extension object."""
        obj = super().get_object(queryset=queryset)
        if obj.is_listed and (
            self.request.user.is_anonymous or not obj.has_maintainer(self.request.user)
        ):
            ExtensionView.create_from_request(self.request, object_id=obj.pk)
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['my_rating'] = ratings.models.Rating.get_for(
                self.request.user.pk, self.object.pk
            )
        return context


class VersionsView(ExtensionQuerysetMixin, ListView):
    model = Version
    paginate_by = 15

    def get_queryset(self):
        """Allow logged in users to view unlisted versions in certain conditions.

        * maintainers should be able to preview their yet unlisted versions;
        * staff should be able to preview yet unlisted versions;
        """
        self.extension_queryset = self.get_extension_queryset()
        self.extension = get_object_or_404(self.extension_queryset, slug=self.kwargs['slug'])
        queryset = self.extension.versions
        if self.request.user.is_staff or self.extension.has_maintainer(self.request.user):
            return queryset.all()
        return queryset.listed

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extension'] = self.extension
        return context


class ManageListView(LoginRequiredMixin, ListView):
    model = Extension
    paginate_by = 15
    template_name = 'extensions/manage/list.html'

    def get_queryset(self):
        return Extension.objects.authored_by(user_id=self.request.user.pk)


class UpdateExtensionView(
    LoginRequiredMixin,
    MaintainedExtensionMixin,
    SuccessMessageMixin,
    UpdateView,
):
    model = Extension
    template_name = 'extensions/manage/update.html'
    form_class = ExtensionUpdateForm
    success_message = "Updated successfully"

    def get_success_url(self):
        self.object.refresh_from_db()
        return self.object.get_manage_url()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        edit_preview_formset = kwargs.pop('edit_preview_formset', None)
        add_preview_formset = kwargs.pop('add_preview_formset', None)
        if not (edit_preview_formset and add_preview_formset):
            if self.request.POST:
                edit_preview_formset = EditPreviewFormSet(
                    self.request.POST, self.request.FILES, instance=self.object
                )
                add_preview_formset = AddPreviewFormSet(
                    self.request.POST,
                    self.request.FILES,
                    extension=self.object,
                    request=self.request,
                )
            else:
                edit_preview_formset = EditPreviewFormSet(instance=self.object)
                add_preview_formset = AddPreviewFormSet(extension=self.object, request=self.request)
        context['edit_preview_formset'] = edit_preview_formset
        context['add_preview_formset'] = add_preview_formset
        return context

    def form_invalid(self, form, edit_preview_formset=None, add_preview_formset=None):
        return self.render_to_response(
            self.get_context_data(
                form=form,
                edit_preview_formset=edit_preview_formset,
                add_preview_formset=add_preview_formset,
            )
        )

    @transaction.atomic
    def form_valid(self, form):
        edit_preview_formset = EditPreviewFormSet(
            self.request.POST, self.request.FILES, instance=self.object
        )
        add_preview_formset = AddPreviewFormSet(
            self.request.POST, self.request.FILES, extension=self.object, request=self.request
        )
        if edit_preview_formset.is_valid() and add_preview_formset.is_valid():
            try:
                edit_preview_formset.save()
                add_preview_formset.save()
                response = super().form_valid(form)
                return response
            except forms.ValidationError as e:
                if 'hash' in e.error_dict:
                    add_preview_formset.forms[0].add_error('source', e.error_dict['hash'])
        return self.form_invalid(form, edit_preview_formset, add_preview_formset)


class VersionDeleteView(
    LoginRequiredMixin,
    MaintainedExtensionMixin,
    DeleteView,
):
    model = Version
    template_name = 'extensions/version_confirm_delete.html'
    form_class = DeleteViewForm

    def get_success_url(self):
        return reverse(
            'extensions:manage-versions',
            kwargs={
                'type_slug': self.kwargs['type_slug'],
                'slug': self.kwargs['slug'],
            },
        )

    def get_object(self, queryset=None):
        return get_object_or_404(
            Version,
            extension__slug=self.kwargs['slug'],
            pk=self.kwargs['pk'],
        )

    def _get_version_from_id(self):
        version_id = self.kwargs['pk']
        version = self.extension.versions.filter(id=version_id).first()
        if version is None:
            raise RuntimeError(
                f'Could not find version {version_id} for extension {self.extension.id}'
            )
        return version

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        version = self._get_version_from_id()
        context['extension_name'] = self.extension.name
        context['version'] = version.version
        context['confirm_url'] = version.get_delete_url()
        return context


class ManageVersionsView(
    LoginRequiredMixin,
    MaintainedExtensionMixin,
    VersionsView,
):
    pass


class NewVersionView(
    LoginRequiredMixin,
    MaintainedExtensionMixin,
    CreateView,
):
    """Upload a file for a new version of existing extension."""

    model = File
    template_name = 'extensions/submit.html'
    form_class = FileForm

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['extension'] = self.extension
        return ctx

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['extension'] = self.extension
        return kwargs

    def get_success_url(self):
        return reverse(
            'extensions:new-version-finalise',
            kwargs={
                'type_slug': self.extension.type_slug,
                'slug': self.extension.slug,
                'pk': self.object.pk,
            },
        )


class NewVersionFinalizeView(LoginRequiredMixin, OwnsFileMixin, CreateView):
    """Finalise a new version of existing extension and send it to review."""

    template_name = 'extensions/new_version_finalise.html'
    form_class = VersionForm

    def form_valid(self, form):
        # Automatically approve new versions, for now.
        # We rely on approval of the first submission, for now.
        self.file.status = self.file.STATUSES.APPROVED
        self.file.save()
        response = super().form_valid(form)
        return response

    def _get_extension(self) -> 'Extension':
        return get_object_or_404(Extension, slug=self.kwargs['slug'])

    def _get_version(self, extension) -> 'Version':
        return Version.objects.update_or_create(
            extension=extension, file=self.file, **self.file.parsed_version_fields
        )[0]

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['instance'] = self._get_version(self.extension)
        return form_kwargs

    def get_initial(self):
        """Return initial values for the version, based on the file."""
        initial = super().get_initial()
        initial['file'] = self.file
        initial.update(**self.file.parsed_version_fields)
        return initial

    def get_success_url(self):
        return self.extension.get_manage_versions_url()


class UpdateVersionView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    """Update release notes for an existing version."""

    template_name = 'extensions/new_version_finalise.html'
    model = Version
    fields = ['release_notes']

    def get_success_url(self):
        return reverse(
            'extensions:versions',
            kwargs={
                'type_slug': self.object.extension.type_slug,
                'slug': self.object.extension.slug,
            },
        )

    def test_func(self) -> bool:
        # Only maintainers are allowed to perform this
        return self.get_object().extension.has_maintainer(self.request.user)
