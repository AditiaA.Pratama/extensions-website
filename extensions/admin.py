import functools
import logging

from django.contrib import admin

from . import models
from common.admin import NoAddDeleteMixin
from extensions.models import Extension, Maintainer, Version


log = logging.getLogger(__name__)


class MaintainerInline(admin.TabularInline):
    model = Maintainer
    raw_id_fields = ('user',)
    extra = 0


class PreviewInline(NoAddDeleteMixin, admin.TabularInline):
    model = Extension.previews.through
    raw_id_fields = ('file',)
    show_change_link = True
    can_add = False
    extra = 0


class VersionInline(NoAddDeleteMixin, admin.TabularInline):
    model = Version
    fields = ('version', 'blender_version_min', 'blender_version_max', 'file')
    raw_id_fields = ('file',)
    show_change_link = True
    extra = 0


class ExtensionAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'type',
        'status',
        'download_count',
        'view_count',
        'average_score',
    )
    list_filter = ('type', 'status')
    search_fields = ('id', '^slug', 'name')
    inlines = (MaintainerInline, PreviewInline, VersionInline)
    readonly_fields = (
        'id',
        'type',
        'name',
        'slug',
        'date_created',
        'date_status_changed',
        'date_approved',
        'date_deleted',
        'date_modified',
        'average_score',
        'text_ratings_count',
        'total_ratings_count',
        'download_count',
        'view_count',
        'website',
    )
    raw_id_fields = ('team',)

    fieldsets = (
        (
            'Details',
            {
                'fields': (
                    ('team',),
                    ('id', 'type'),
                    (
                        'date_created',
                        'date_status_changed',
                        'date_approved',
                        'date_modified',
                        'date_deleted',
                    ),
                    'name',
                    'slug',
                    'description',
                    'status',
                ),
            },
        ),
        (
            'Support',
            {
                'fields': ('website', 'support'),
            },
        ),
        (
            'Stats',
            {
                'fields': (
                    ('average_score', 'text_ratings_count', 'download_count', 'view_count'),
                ),
            },
        ),
    )

    def get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            return functools.update_wrapper(wrapper, view)

        urlpatterns = super().get_urls()
        custom_urlpatterns = []
        return custom_urlpatterns + urlpatterns


class VersionAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'extension',
        'download_count',
        'average_score',
    )
    list_filter = (
        'file__status',
        'blender_version_min',
        'blender_version_max',
        'licenses',
        'tags',
        'permissions',
    )
    search_fields = ('id', 'extension__slug', 'extension__name')
    raw_id_fields = ('extension', 'file')
    readonly_fields = (
        'id',
        'tagline',
        'date_created',
        'date_modified',
        'average_score',
        'download_count',
    )

    fieldsets = (
        (
            'Details',
            {
                'fields': (
                    'id',
                    'tagline',
                    ('date_created', 'date_modified'),
                    'extension',
                    'version',
                    'blender_version_min',
                    'blender_version_max',
                    'release_notes',
                    'licenses',
                    'tags',
                    'file',
                    'permissions',
                ),
            },
        ),
        (
            'Stats',
            {
                'fields': (
                    'average_score',
                    'download_count',
                ),
            },
        ),
    )

    def get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            return functools.update_wrapper(wrapper, view)

        urlpatterns = super().get_urls()
        custom_urlpatterns = []
        return custom_urlpatterns + urlpatterns


class MaintainerAdmin(admin.ModelAdmin):
    model = Maintainer
    list_display = ('extension', 'user', 'date_deleted')
    readonly_fields = ('extension', 'user', 'date_deleted')


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'url')


admin.site.register(models.Extension, ExtensionAdmin)
admin.site.register(models.Version, VersionAdmin)
admin.site.register(models.Maintainer, MaintainerAdmin)
admin.site.register(models.License, LicenseAdmin)
