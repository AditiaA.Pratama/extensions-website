![blender-extensions-logo](common/static/common/images/logo.svg)

* [Blender Extensions](#blender-extensions)
* [Requirements](#requirements)
  * [Development](#development)
      * [Extra requirements](#extra-requirements)
      * [Setup](#setup)
      * [Blender ID](#blender-id)
  * [Pre-commit hooks](#pre-commit-hooks)
  * [Testing](#testing)
* [Deploy](#deploy)

# Blender Extensions

Blender Extensions platform, heavily inspired by Mozilla's https://github.com/mozilla/addons-server.

# Requirements

* Python 3.10

## Development

### Extra requirements

* `virtualenv`

### Setup

Checkout Blender Web Assets submodule first:

    git submodule update --init --recursive

Create and activate a virtual environment using your favourite method, e.g.

    virtualenv .venv -p python
    source .venv/bin/activate

Install required packages:

    pip install -r requirements_dev.txt

Create the database tables and load some basic data to work with using the following commands:

    ./manage.py migrate
    ./manage.py loaddata **/fixtures/*.json

It's also possible to generate fake add-on data using the following command:

    ./manage.py generate_fake_data

Run development server

    ./manage.py runserver 8111

Update `/etc/hosts` to point to `extensions.local`, e.g.:

    127.0.0.1	extensions.local

Now http://extensions.local:8111 should be ready to work with and it should be possible to
log into http://extensions.local:8111/admin/ with `admin`/`admin`.

### Blender ID

All the configuration of this web app is done via environment variables
(there's only one `settings.py` regardless of an environment).

Blender Extensions, as all other Blender web services, uses Blender ID.
To configure OAuth login, first create a new OAuth2 application in Blender ID with the following settings:

* Redirect URIs: `http://extensions.local:8111/oauth/authorized`
* Client type: "Confidential";
* Authorization grant type: "Authorization code";
* Name: "Blender Extensions Dev";

Then copy client ID and secret and save them as `BID_OAUTH_CLIENT` and `BID_OAUTH_SECRET` into a `.env` file:

    export BID_OAUTH_CLIENT=<CLIENT ID HERE>
    export BID_OAUTH_SECRET=<SECRET HERE>

Run the dev server using the following command:

    source .env && ./manage.py runserver 8111

#### Webhook

Blender Extensions can receive account modifications such as badge updates via a webhook,
which has to be configured in Blender ID admin separately from the OAuth app.

In Admin › Blender-ID API › Webhooks click `Add Webhook` and set the following:

* Name: "Blender Extensions Dev";
* URL: `http://extensions.local:8111/webhooks/user-modified/`;
* App: choose the app created in the previous step;

Then copy webhook's secret into the `.env` file as `BID_WEBHOOK_USER_MODIFIED_SECRET`:

    export BID_WEBHOOK_USER_MODIFIED_SECRET=<WEBHOOK SECRET HERE>

**N.B.**: the webhook view delegates the actual updating of the user profile
to a background task, so in order to see the updates locally, start the processing of
tasks using the following:

    source .env && ./manage.py process_tasks

#### Blender ID and staging/production

The above steps use local development setup as example.
For staging/production the steps are the same, the only differences being
the names of the app and the webhook,
and `http://extensions.local:8111` being replaced with the appropriate base URL.

## Pre-commit hooks

Make sure to enable pre-commit hooks after installing requirements from `requirements_dev.txt`:

    pre-commit install

This will enable formatting pre-commit checks for Django templates, Python and JS modules.

## Testing

To simply run the test suit use

    ./manage.py test

To run tests and generate a coverage report use

    coverage run manage.py test && coverage html

and then open `htmlcov/index.html` with your favourite browser.

# Deploy

See [playbooks](/playbooks/).

# Feature Flags

At the moment there are the following feature flags:
* `is_alpha`
* `is_beta`

To create them run

    ./manage.py waffle_switch --create is_alpha on

    ./manage.py waffle_switch --create is_beta off

They can optionally be setup on the deployment platform to set the release stage of the site.
These settings will be removed once the site is officially launched.
