Dear {% firstof user.full_name user.email %},
{% block content %}{% endblock content %}

Manage your profile: {{ profile_url }}

--
Kind regards,

Blender Extensions Team
