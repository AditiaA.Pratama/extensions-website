from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from ratings.models import Rating


@receiver(pre_save, sender=Rating)
def _set_extension(sender, instance, *args, **kwargs):
    if not instance.extension_id:
        instance.extension_id = instance.version.extension_id


@receiver(post_save, sender=Rating)
def _update_rating_counters(sender, instance, *args, **kwargs):
    extension = instance.version.extension
    extension.recalculate_average_score()

    version = instance.version
    version.recalculate_average_score()
