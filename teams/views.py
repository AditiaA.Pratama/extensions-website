"""Team pages."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

import teams.models


class TeamsView(LoginRequiredMixin, ListView):
    model = teams.models.Team

    def get_queryset(self):
        return self.request.user.teams.all()
