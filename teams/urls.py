from django.urls import path

import teams.views

app_name = 'teams'
urlpatterns = [
    path('settings/teams/', teams.views.TeamsView.as_view(), name='list'),
]
