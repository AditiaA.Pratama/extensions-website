from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = 'Authentication and authorization'

    def ready(self) -> None:
        import users.signals  # noqa: F401
