from typing import Dict
import logging

from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save
from django.dispatch import receiver

from blender_id_oauth_client import signals as bid_signals

from users.blender_id import BIDSession

User = get_user_model()
bid = BIDSession()
logger = logging.getLogger(__name__)


@receiver(pre_save, sender=User)
def _record_changes(sender: object, instance: User, **kwargs: object) -> None:
    was_changed, old_state = instance.pre_save_record()

    instance.record_status_change(was_changed, old_state, **kwargs)


@receiver(bid_signals.user_created)
def update_user(
    sender: object, instance: User, oauth_info: Dict[str, str], **kwargs: object
) -> None:
    """Update a User when a new OAuth user is created.

    Copy 'full_name' from the received 'oauth_info' and attempt to copy avatar from Blender ID.
    """
    instance.full_name = oauth_info.get('full_name') or ''
    instance.confirmed_email_at = oauth_info.get('confirmed_email_at')
    instance.save()

    bid.copy_avatar_from_blender_id(user=instance)
    bid.copy_badges_from_blender_id(user=instance)
