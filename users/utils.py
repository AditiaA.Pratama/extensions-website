from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()


def get_task_user():
    """
    Returns a user object. This user is suitable for assigning to
    cron jobs or long running tasks.
    """
    return User.objects.get(pk=settings.TASK_USER_ID)
